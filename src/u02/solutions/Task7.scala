package u02.solutions

object Task7 extends App {
  sealed trait Shape
  object Shape {
    case class Rectangle(b: Double, h: Double) extends Shape
    case class Circle(r: Double) extends Shape
    case class Square(l: Double) extends Shape

    def perimeter(shape: Shape): Double = shape match {
      case Rectangle(b, h) => (b + h) * 2
      case Circle(r) => r * 2 * Math.PI
      case Square(l) => l * 4
    }

    def area(shape: Shape): Double = shape match {
      case Rectangle(b, h) => b * h
      case Circle(r) => Math.PI * r * r
      case Square(l) => l * l
    }
  }
}
