package u02.solutions

object Task1 extends App {

  val parityVal: Int => String = x => x % 2 match {
    case 0 => "even"
    case _ => "odd"
  }

  def parityDef(x: Int): String = x % 2 match {
    case 0 => "even"
    case _ => "odd"
  }

  println(parityVal(3))
  println(parityDef(3))
  println(parityVal(2))
  println(parityDef(2))
}
