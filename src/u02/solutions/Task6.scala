package u02.solutions

object Task6 extends App {
  def fibonacci(n: Int): Int = n match {
    case x if x <= 1 => x
    case _ => fibonacci(n - 1) + fibonacci(n - 2)
  }

  println(fibonacci(5))
}
