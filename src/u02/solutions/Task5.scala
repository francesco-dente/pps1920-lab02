package u02.solutions

object Task5 extends App {
  def composeInt(f: Int => Int, g: Int => Int): Int => Int = x => f(g(x))
  def compose[A, B, C](f: B => C, g: A => B): A => C = x => f(g(x))

  val addFiveThenDouble: Int => Int = compose[Int, Int, Int](_ * 2, _ + 5)

  println(addFiveThenDouble(3)) // 16
}
