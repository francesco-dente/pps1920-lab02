package u02.solutions

object Task2 extends App {
  val negVal: (String => Boolean) => String => Boolean = predicate => !predicate(_)

  def negDef(predicate: String => Boolean): String => Boolean = !predicate(_)

  val empty: String => Boolean = _ == ""
  val notEmptyVal = negVal(empty);
  val notEmptyDef = negDef(empty);

  println(notEmptyVal("foo"))
  println(notEmptyDef("foo"))
  println(notEmptyVal(""))
  println(notEmptyDef(""))
}
