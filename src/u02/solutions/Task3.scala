package u02.solutions

object Task3 extends App {
  def neg[T](predicate: T => Boolean): T => Boolean = !predicate(_)

  val empty: String => Boolean = _ == ""
  val notEmpty = neg(empty);

  println(notEmpty("foo"))
  println(notEmpty(""))
}
