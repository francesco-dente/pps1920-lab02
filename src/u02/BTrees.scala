package u02

object BTrees extends App {

  // A custom and generic binary tree of elements of type A
  sealed trait Tree[A]
  object Tree {
    case class Leaf[A](value: A) extends Tree[A]
    case class Branch[A](left: Tree[A], right: Tree[A]) extends Tree[A]

    def traverse[A, R](t: Tree[A], combine: (R, R) => R, mapper: A => R): R = t match {
      case Branch(l, r) => combine(traverse(l, combine, mapper), traverse(r, combine, mapper))
      case Leaf(e) => mapper(e)
    }

    def size[A](t: Tree[A]): Int = traverse[A, Int](t, _ + _, _ => 1)

    def find[A](t: Tree[A], elem: A): Boolean = traverse[A, Boolean](t, _ || _, _ == elem)

    def count[A](t: Tree[A], elem: A): Int = traverse[A, Int](t, _ + _, {
      case e if e == elem => 1
      case _ => 0
    })
  }

  import Tree._
  val tree = Branch(Branch(Leaf(1),Leaf(2)),Leaf(1))
  println(tree, size(tree)) // ..,3
  println( find(tree, 1)) // true
  println( find(tree, 4)) // false
  println( count(tree, 1)) // 2
}
