package u02.solutions

import org.junit.jupiter.api.Test
import org.junit.jupiter.api.Assertions._
import u02.solutions.Task7._
import u02.solutions.Task7.Shape._

class ShapesTest {
  val square: Shape = Square(3)
  val circle: Shape = Circle(3)
  val rect: Shape = Rectangle(3, 2)
  val delta: Double = 0.001

  @Test def testPerimeter() {
    assertEquals(12.0, perimeter(square), delta)
    assertEquals(10.0, perimeter(rect), delta)
    assertEquals(18.849, perimeter(circle), delta)
  }

  @Test def testArea() {
    assertEquals(9.0, area(square), delta)
    assertEquals(6.0, area(rect), delta)
    assertEquals(28.2735, area(circle), delta)
  }
}
