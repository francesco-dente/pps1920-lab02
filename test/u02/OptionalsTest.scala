package u02

import org.junit.jupiter.api.Assertions._
import org.junit.jupiter.api.Test

class OptionalsTest {
  import Optionals._
  import Optionals.Option._
  val five = Some(5)
  val ten = Some(10)
  val empty = None[Int]()

  @Test def testIsEmpty() {
    assertTrue(isEmpty(empty))
    assertFalse(isEmpty(five))
  }

  @Test def testGetOrElse() {
    assertEquals(5, getOrElse(five, 4))
    assertEquals(4, getOrElse(empty, 4))
  }

  @Test def testFlatMap() {
    assertEquals(Some(3), flatMap(five)(a => Some(a - 2)))
    assertEquals(None[Int](), flatMap(five)(a => None[Int]()))
    assertEquals(None[Int](), flatMap(empty)(a => Some(a - 2)))
    assertEquals(None[Int](), flatMap(empty)(a => None[Int]()))
  }

  @Test def testFilter() {
    assertEquals(Some(5), filter(five)(_ > 0))
    assertEquals(None[Int](), filter(five)(_ < 0))
    assertEquals(None[Int](), filter(empty)(_ > 0))
  }

  @Test def testMap() {
    assertEquals(Some(6), map(five)(_ + 1))
    assertEquals(None[Int](), map(empty)(_ + 1))
  }

  @Test def testMap2() {
    assertEquals(Some(15), map2(five, ten)(_ + _))
    assertEquals(None[Int](), map2(five, empty)(_ + _))
    assertEquals(None[Int](), map2(empty, ten)(_ + _))
    assertEquals(None[Int](), map2(five, empty)(_ + _))
  }
}
